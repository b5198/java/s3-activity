package com.zuitt.batch193;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int num = 0;
        int j = 1;
        int i = 1;

        try{
            System.out.println("Input an integer whose factorial will be computed");
            num = in.nextInt();
        } catch (InputMismatchException e){
            System.out.println("Input is not a number");
        } catch (Exception e){ // Exception to catch any errors (generic error)
            System.out.println("Invalid Input");
        } finally {
            if (num != 0){
                System.out.println("The number you entered is: " + num);
            }
        }

        while(i <= num){
            j = j * i;
            i++;
        }

        System.out.println("The factorial of " + num + " is: " + j);

    }
}
